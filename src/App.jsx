
import { useEffect, useState } from "react";
import './App.css'


function IconoPokemon(props) {
  return (
    <img
      width="80px"
      title={props.nombre}
      onClick={props.onClick}
      src={"https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/" + props.nombre.toLowerCase() + ".png"}
      alt={props.nombre}
    />
  );
}

function FotoPokemon({ nombre }) {
  return (
    <img
      width="400px"
      src={"https://img.pokemondb.net/artwork/large/" + nombre.toLowerCase() + ".jpg"}
      alt={nombre}
    />
  );
}

function PokeGros({ item }) {
  if (!item) return <>...</>;
  return (
    <div className="poke-gros">
      <FotoPokemon nombre={item.nombre} />
      <h1>{item.nombre}</h1>
      <h2>{item.caracter}</h2>
    </div >
  );
}


function App() {

  const [pokemons, setPokemons] = useState([]);
  const [poke, setPoke] = useState(null);

  function getPokemons() {
    fetch("http://localhost:3000/api/pokemons")
      .then(results => results.json())
      .then(results => setPokemons(results.data))
      .catch(err => console.log(err));
  }

  useEffect(() => {
    getPokemons();
  }, [])

  if (!pokemons.length) {
    return <>no data</>;
  }

  return (
    <>
      <div className="container">
        <div className="pokemons-petits">
          {pokemons.map((elpoke, idx) => <IconoPokemon key={idx} onClick={() => setPoke(elpoke)} nombre={elpoke.nombre} />)}
        </div>
        <PokeGros item={poke} />
      </div>
    </>
  );
}


export default App
